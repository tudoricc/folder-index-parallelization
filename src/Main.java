import java.io.*;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import pools.*;
import solutions.*;

class SpecialWorker extends Thread {
	MapWorkPool mapwp;
	ReduceWorkPool redwp;
	CompareWorkPool cmpwp;
	int count = 0;
	public SpecialWorker(MapWorkPool workpool,ReduceWorkPool red , CompareWorkPool cmp) {
		this.mapwp = workpool;
		this.redwp = red;
		this.cmpwp = cmp;

	}
	//functie care imi verifica daca un caracter face parte din setul de separatori
	public static boolean IsDelimitator(char litera){
		boolean res2 = false;
		if ((int)litera>=0 && (int)litera <= 47){
			res2 = true;
		}
		else if((int)litera >=58 && (int)litera<=64){
			res2 = true;

		}
		else if((int)litera >= 91 && (int)litera<=96){
			res2 = true;
		}
		else if((int)litera>=123 && (int)litera<=136){
			res2 = true;
		} 
		else if(litera == ' '){
			res2 = true;
		}


		return res2;



	}
	/**
	 * Procesarea unei solutii partiale. Aceasta poate implica generarea unor
	 * noi solutii partiale care se adauga in workpool folosind putWork().
	 * Daca s-a ajuns la o solutie finala, aceasta va fi afisata.
	 * @throws FileNotFoundException 
	 */
	//functie care prelucreaza un task de tipul map
	void reduceMapPartialSolution(MapPartialSolution ps) throws IOException {
		int end = ps.endOffset;
		int start = ps.startOffset;
		String file = ps.nameDocument;
		byte buf[] = new byte[end-start+1];
		RandomAccessFile input = new RandomAccessFile(new File(file), "r");

		ConcurrentHashMap<String, Integer> hashMapReduce = new ConcurrentHashMap<>();


		input.seek(start);
		input.read(buf);
		int pos = 0;
		//verific daca incepe fragmentul in mijhlocul cuvantului sau nu
		//daca da ,ma uit in dreapta,daca nu in stanga.
		if(start!=0 &&!IsDelimitator((char)buf[0])){

			byte [] buf2 = new byte[buf.length+1];
			input.seek(start-1);
			input.read(buf2);
			if(IsDelimitator((char)buf2[0])){
				pos = 0;
			}
			else{
				for ( int i = 0 ; i < buf.length ; i ++ ) {
					if  (!IsDelimitator((char)buf[i])){
						pos++;
					}
					else{
						break;
					}
				}
			}
		}


		String cuvant="";

		/*		for (int i = 0 ; i < buf.length ; i++){
			cuvant+=(char)buf[i];

		}
		System.out.println("cuvantul inainte este:" + cuvant);
		 */

		buf = new byte[buf.length-pos];

		input.seek(start+pos);
		input.read(buf);
		//System.out.println(Main.fileAndMaxSize.get(file));
		//ma uit daca se termina in caracter sau in delimitator si ma uit in dreapta
		if(end!=Main.fileAndMaxSize.get(file)){
			while (!IsDelimitator((char)buf[buf.length-1]) /*&& end!=Main.fileAndMaxSize.get(file)*/){
				buf = new byte[buf.length+1];
				input.seek(start+pos);
				input.read(buf);
			}
		}

		String cuvant2 = "";
		/*for (int i = 0 ; i < buf.length ; i++){
			cuvant2+=(char)buf[i];

		}
		 */		//System.out.println("cuvantul dupa este:" + cuvant2);
		int i = 0;
		int startPrelucrare = 0;

		Vector<String> words = new Vector<String>();
		//acum incep sa iau cuvintele din fragment-dupa prelucrare
		for(int m = 0 ;m<buf.length;m++){
			if(!IsDelimitator((char)buf[m])){
				break;
			}
			else{
				startPrelucrare++;
			}
		}
		i=startPrelucrare;

		while(startPrelucrare<buf.length){
			if (end == Main.fileAndMaxSize.get(file)){

				startPrelucrare = buf.length-1;
				for(int k = i ; k<=startPrelucrare ; k++){
					cuvant2+=(char)buf[k];
				}
				if(cuvant2.equals("")){

				}
				else{
					
					if(hashMapReduce.get(cuvant2.toLowerCase())==null){
						hashMapReduce.put(cuvant2.toLowerCase(), 1);
					}
					else{
						hashMapReduce.replace(cuvant2.toLowerCase(),hashMapReduce.get(cuvant2.toLowerCase())+1);
					}
//					words.add(cuvant2.toLowerCase());
				}
				cuvant2 = new String();;
			}
			if(!IsDelimitator((char)buf[startPrelucrare])){
				startPrelucrare++;
			}
			else{

				for(int k = i ; k<startPrelucrare ; k++){
					cuvant2+=(char)buf[k];
				}
				if(cuvant2.equals("")){

				}
				else{
					if(hashMapReduce.get(cuvant2.toLowerCase())==null){
						hashMapReduce.put(cuvant2.toLowerCase(), 1);
					}
					else{
						hashMapReduce.replace(cuvant2.toLowerCase(),hashMapReduce.get(cuvant2.toLowerCase())+1);
					}
				}
				cuvant2 = new String();
				i = startPrelucrare+1;
				startPrelucrare++;
			}

		}
		/*System.out.println("\n--------------------------------------\nCuvintele din fragment sunt\n---------------------");
		for(String word:words){
			System.out.println(word);
		}

		System.out.println("\nSi aici se termina\n--------------------------------------------------\n");
		 */
		//construiesc hashmap-ul acestui fragment
	/*	for(String word:words){

			int noOccurences = Collections.frequency(words, word);
			hashMapReduce.put(word, noOccurences);
		}*/

		ReducePartialSolution redPartialSolution = new ReducePartialSolution(file, hashMapReduce);


		Main.redWP.putWork(redPartialSolution);
		//this.redwp.
		int totalWords = 0;


	}

	//functie care prelucreaza un task de tip-ul reduce
	void processRedPartialSolution(ReducePartialSolution redps){
		//	System.out.println("Macar ajunge aici");
		ConcurrentHashMap< String, Integer> tabel = redps.occurences;
		//System.out.println("Zi-mi ca ajunge aici"+this.getName());
		//System.out.println(tabel.toString());

		//	System.out.println("tabelul ajusn in reduce este " + tabel);
		String nameOfDocument = redps.nameDocument;

		//daca nu am in hash-ul mare[cel din main] acest document il adaug pur si simplu
		if(Main.allFilesHashtable.get(nameOfDocument)==null){
			Main.allFilesHashtable.put(nameOfDocument, tabel);
		}
		else{
			//inseamna ca mai avem aici in tabel intrarea acesatui fisier
			//mai avem 2 cazuri.cazul in care deja are cuvantul acolo si cazul in care nu are
			for(Map.Entry<String, Integer>entry : tabel.entrySet()){
				//daca nu exista acel cuvant ipentru acel document il adaug pur si simplu
				if(Main.allFilesHashtable.get(nameOfDocument).get(entry.getKey())==null){
					Main.allFilesHashtable.get(nameOfDocument).put(entry.getKey(), entry.getValue());
				}
				else{
					//inseamna ca nu mai am acest cuvant in tabela documentului respectiv.ce fac e sa updatez
					//Main.allFilesHashtable.get(nameOfDocument).replace(entry.getKey(), entry.getValue()+Main.allFilesHashtable.get(nameOfDocument).get(entry.getValue()));
					ConcurrentHashMap<String, Integer>oldTabel = Main.allFilesHashtable.get(nameOfDocument);
					int newOcc = oldTabel.get(entry.getKey())+entry.getValue();
					//oldTabel.get(entry.getValue())
					oldTabel.replace(entry.getKey(), newOcc);
					Main.allFilesHashtable.replace(nameOfDocument, oldTabel);

				}



			}

		}
		//		System.out.println(Main.allFilesHashtable.toString()+"Uitate mai jos");
		//
	}
	double calcAppearances(double selfOcc , double totalWords ){
		double res = 0;
		res = (selfOcc/totalWords)*100;
		return res;
	}

	//procesare solutiile partiale de tipul compare
	void processCmpPartialSolution(ComparePartialSolution cmpps){
		String doc1 = cmpps.nameDocument1;
		String doc2 = cmpps.nameDocument2;
		ConcurrentHashMap<String , Integer	> hashDoc1 = cmpps.totalOccurences1;
		ConcurrentHashMap<String, Integer> hashDoc2 = cmpps.totalOccurences2;
		DecimalFormat format3cifre = new DecimalFormat(".000");
		format3cifre.setRoundingMode(RoundingMode.DOWN);
		//System.out.println(hashDoc1.toString());
		//System.out.println("ajunge aici in compare");
		double sim =0;
		int countWordsDoc1 = 0;

		for (Map.Entry<String, Integer>values : hashDoc1.entrySet()){
			countWordsDoc1+=values.getValue();

		}

		int countWordsDoc2 = 0;
		for(Map.Entry<String, Integer>values2 : hashDoc2.entrySet()){
			countWordsDoc2 += values2.getValue();
		}



		//ma intereseaza doar cuvintele care apar in ambele.nu are rost sa ma uit si la result pentru ca ele
		//adunate nu vor modifica similitudinea pentru cele 2 fisiere
		for (Map.Entry<String, Integer>value : hashDoc1.entrySet()){
			double occ = value.getValue();//aici iau fiecare aparitie pentru fiecare cuvant;
			//	System.out.println(occ+ " " + values.getKey());
			//	System.out.println(value.getKey() + " apare de " + occ + "in " + hashDoc1);


			double res1 = Double.parseDouble(format3cifre.format((occ/countWordsDoc1)*100));
			double res2 = 0;;
			double occ2 = 0;
			if (hashDoc2.get(value.getKey())!=null){
				occ2 = hashDoc2.get(value.getKey());

				res2 = Double.parseDouble(format3cifre.format((occ2/countWordsDoc2)*100));
				// System.out.println(res2+ " sddsa " + res1);
			}

			sim+= res1*res2;
		}



		if(sim/100 > Main.SimilitudinyDegree){
			Main.listaFinala.add(new Truplu(doc1, doc2, sim/100));
		}


	}



	int returnComb(int no){
		int prod = 1;
		for (int i = 1 ; i<=no;i++){
			prod*=i;
		}
		return prod;
	}
	public void run() {
		System.out.println("Thread-ul worker " + this.getName() + " a pornit...");
		while (true) {
			//if(Main.moment==0){
			MapPartialSolution mapPS = mapwp.getWork();
			//vad daca procesez sau nu task-ul de tip map
			if(Main.mapWP.tasks.size()!=0){
				try {
					reduceMapPartialSolution(mapPS);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else{
				//trec la task-ul de tip reduce
				//System.out.println("mACAR AJUNGE AICI");
				Main.redWP = this.redwp;
				//Main.moment = 1;
				ReducePartialSolution redPS = this.redwp.getWork();
				if(redPS!=null){
					processRedPartialSolution(redPS);
				}
				else{
					//trec la cel de tipul compare
					ComparePartialSolution cmpPS = this.cmpwp.getWork();

					if (cmpPS!=null){
						//	System.out.println("NU AJUNGE AICI");
						processCmpPartialSolution(cmpPS);
					}
					else{
						return;
					}
				}


			}


		}
	}
}


public class Main {
	static int noThreads ;
	static String inputFile ; 
	static String outputFile;
	static int sizeSplit;
	static float SimilitudinyDegree;
	static int noFiles;
	static ArrayList<String> fileNames  = new ArrayList<String>();
	static HashMap<String, Long>fileAndMaxSize = new HashMap<String,Long>();
	static HashMap<String, Long>fileAndMinSize = new HashMap<String,Long>();
	static int moment = 0;
	static Vector<Truplu>listaFinala = new Vector<Truplu>();
	static boolean fullMap = false;
	static ConcurrentHashMap<String, ConcurrentHashMap<String, Integer>> allFilesHashtable = new ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>>();

	static MapWorkPool mapWP;
	static ReduceWorkPool redWP;
	static CompareWorkPool cmpWP;
	/*pe linia I: dimensiunea D (in octeti) a fragmentelor in care se vor imparti fisierele
	pe linia II: numarul X reprezentÃ¢nd â€œpragul de similaritateâ€� (ex.: vreau sa mi se returneze perechile de documente cu gradul de similaritate mai mare de X)
	pe linia III: numarul ND de documente de tip text de indexat si comparat
	pe urmatoarele ND linii: numele celor ND documente (cÃ¢te unul pe linie)*/


	//citesc de la linia de comanda
	public void readFromInput() throws NumberFormatException, IOException{
		BufferedReader a = new BufferedReader(new FileReader(inputFile));
		sizeSplit = Integer.parseInt(a.readLine());
		//System.out.println(sizeSplqit);

		SimilitudinyDegree = Float.parseFloat(a.readLine());
		noFiles = Integer.parseInt(a.readLine());
		String line;
		int linesToRead = noFiles;
		while((line = a.readLine())!= null && linesToRead>0 ){
			fileNames.add(line);
			linesToRead--;
		}


	}

	//creez o lista de task-uri de tip Map
	public LinkedList<MapPartialSolution> createMapSolution() throws IOException{
		LinkedList<MapPartialSolution> tasks = new LinkedList<MapPartialSolution>();
		for(String a : fileNames){

			RandomAccessFile file = new RandomAccessFile(new File(a), "r");
			int start = 0;
			long end = file.length();
			fileAndMaxSize.put(a, end);

			int startSolution = start;
			int endSolution = start + sizeSplit-1;

			while (startSolution <=end && endSolution<= end){
				//cazul obisnuit

				//MapPartialSolution a = new MapPartialSolution(doc, startSolution, end)
				MapPartialSolution solutiePartiala = new MapPartialSolution(a, startSolution, endSolution);
				startSolution = endSolution+1;
				endSolution = startSolution+sizeSplit-1;
				tasks.add(solutiePartiala);
			}
			if (endSolution>= end){
				//cazul in care nu am fix cate caractere trebuie la sfarsit si pun mai putine
				MapPartialSolution solFinala = new MapPartialSolution(a,startSolution,(int)end);
				tasks.add(solFinala);
			}


		}
		return tasks;

	}



	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws NumberFormatException, IOException, InterruptedException{
		//	/ NT (numarul de thread-uri worker), 
		//numele unui fisier de intrare si numele unui fisier de iesire (in aceasta ordine).	
		mapWP = new MapWorkPool(noThreads);
		redWP = new ReduceWorkPool(noThreads);
		cmpWP = new CompareWorkPool(noThreads);
		Vector<SpecialWorker> workersMap = new Vector<SpecialWorker>();

		noThreads = Integer.parseInt(args[0]);
		inputFile = args[1];
		outputFile = args[2];

		Main a = new Main();
		a.readFromInput();

		LinkedList<MapPartialSolution> res = a.createMapSolution();
		//System.out.println(fileAndMaxSize.toString());
		for (MapPartialSolution copy : res){
			mapWP.putWork(copy);
		}
		//mapWP.showPool();

		for(int i = 0 ; i< noThreads;i++){
			SpecialWorker worker = new SpecialWorker(mapWP, redWP, cmpWP);
			workersMap.add(worker);
		}
		for(SpecialWorker wrk : workersMap ){
			wrk.start();

		}
		for(SpecialWorker wrk : workersMap ){
			wrk.join();

		}




		Vector<SpecialWorker>workersReduce = new Vector<SpecialWorker>(noThreads);
		Main.moment = 1;

		//creez workeri pentru Reduce
		for(int i = 0 ; i< noThreads;i++){
			SpecialWorker worker = new SpecialWorker(mapWP, redWP, cmpWP);
			workersReduce.add(worker);
		}
		for(SpecialWorker wrk : workersReduce ){
			wrk.start();
			//System.out.println("DAR AICI");

		}
		for(SpecialWorker wrk : workersReduce ){
			wrk.join();

		}
		//creez taskurile de tip COMPARE
		for(int i = 0 ; i < fileNames.size();i++){
			for ( int j = i+1 ; j < fileNames.size()&&j!=i ; j++){
				ComparePartialSolution cmpNew = new ComparePartialSolution(fileNames.get(i), allFilesHashtable.get(fileNames.get(i)), fileNames.get(j), allFilesHashtable.get(fileNames.get(j)));
				if (!cmpWP.tasks.contains(cmpNew)){
					cmpWP.putWork(cmpNew);
				}

			}
		}
		//System.out.println(cmpWP.tasks.size());



		//System.out.println("dsadssda"+Main.allFilesHashtable.toString());
		//		Main.moment = 2;

		//creez workeri pentru compare
		Vector<SpecialWorker> workersCompare = new Vector<SpecialWorker>(noThreads);
		for(int i = 0 ; i< noThreads;i++){
			SpecialWorker worker = new SpecialWorker(mapWP, redWP, cmpWP);
			workersCompare.add(worker);

		}

		//procesez taskurile de compare
		for(SpecialWorker wrk : workersCompare ){
			wrk.start();
			//System.out.println("DAR AICI");

		}
		for(SpecialWorker wrk : workersCompare){
			wrk.join();

		}

		//sortez lista finala pentru fisierul de output
		Collections.sort(listaFinala, new Comparator<Truplu>() {

			@Override
			public int compare(Truplu o1, Truplu o2) {
				// TODO Auto-generated method stub
				if(o1.sim < o2.sim ){
					return 1;
				}
				else if(o1.sim>o2.sim){
					return -1;
				}
				else {
					return 0;
				}
			}
		});


		//scriu in fisierul de output
		BufferedWriter output = new BufferedWriter(new FileWriter(outputFile));
		for (int i = 0 ; i<listaFinala.size();i++){

			output.write(listaFinala.get(i).toString());
			//}
		}
		output.close();
	}


}
